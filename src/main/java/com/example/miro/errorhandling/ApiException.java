package com.example.miro.errorhandling;

public class ApiException extends RuntimeException {

    private ApiError error;
    private static final long serialVersionUID = -4954041942323025562L;

    public ApiException(ApiError error, Throwable cause) {
        super(error.getMessage(), cause);
        this.error=error;
    }

    public ApiException(ApiError error) {
        super(error.getMessage());
        this.error=error;
    }

    public ApiError getError() {
        return error;
    }
}