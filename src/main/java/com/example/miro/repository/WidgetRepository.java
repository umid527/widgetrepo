package com.example.miro.repository;

import com.example.miro.dto.WidgetDto;
import com.example.miro.model.Widget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WidgetRepository extends JpaRepository<Widget, Integer> {

    @Query("SELECT new com.example.miro.dto.WidgetDto(w.coordinationX,w.coordinationY,w.indexZ,w.width,w.height,w.id,w.lastModificationDate) FROM Widget w order by w.indexZ asc ")
    List<WidgetDto> findAllByOrderByIndexZAscAsWidgetDto();

    @Query("SELECT coalesce(max(indexZ+1), 0) FROM Widget")
    Integer possibleUsableIndexZ();

    boolean existsByIndexZ(Integer indexZ);

    @Modifying
    @Query("update Widget w set w.indexZ = w.indexZ+1 where w.indexZ >= :indexZ")
    void shiftAllIndexZAfter(@Param("indexZ") Integer indexZ);

}
