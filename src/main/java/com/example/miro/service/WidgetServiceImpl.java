package com.example.miro.service;


import com.example.miro.dto.WidgetDto;
import com.example.miro.dto.WidgetInsertDto;
import com.example.miro.errorhandling.ApiError;
import com.example.miro.errorhandling.ApiException;
import com.example.miro.model.Widget;
import com.example.miro.repository.WidgetRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class WidgetServiceImpl implements WidgetService {

    private static final Logger logger = LogManager.getLogger(WidgetServiceImpl.class);


    private final WidgetRepository widgetRepository;

    public WidgetServiceImpl(WidgetRepository widgetRepository) {
        this.widgetRepository = widgetRepository;
    }

    @Transactional
    @Override
    public WidgetDto createWidget(WidgetInsertDto widgetDto) {
        logger.debug("create widget call wirh widgetDto :{}",widgetDto);
        Widget widget= widgetDto.toConvertWidget();
        if(widget.getIndexZ()==null){
            widget.setIndexZ(widgetRepository.possibleUsableIndexZ());
        }else {
            if(widgetRepository.existsByIndexZ(widget.getIndexZ())){
                widgetRepository.shiftAllIndexZAfter(widget.getIndexZ());
            }
        }
        Widget savedWidget=widgetRepository.save(widget);
        return savedWidget.toConvertWidgetDto();
    }


    @Override
    public WidgetDto findById(int widgetId) throws ApiException {
        logger.debug("find by Ud call wirh  :{}",widgetId);
        Widget widget = widgetRepository.findById(widgetId)
                .orElseThrow(() -> new ApiException(new ApiError("Widget not found for this id : " + widgetId)));
        return widget.toConvertWidgetDto();
    }

    @Override
    @Transactional
    public WidgetDto updateWidget(int widgetId, WidgetInsertDto widgetDto) {
        logger.debug("update widget call with id  :{} and :{} ",widgetId,widgetDto);

        Widget oldWidget=widgetRepository.findById(widgetId)
                .orElseThrow(() -> new ApiException(new ApiError("Widget not found for this id : " + widgetId)));
        Widget widget=widgetDto.toConvertWidget();


        if(!oldWidget.getIndexZ().equals(widget.getIndexZ())){
            if(widget.getIndexZ()==null){
                oldWidget.setIndexZ(widgetRepository.possibleUsableIndexZ());
            }else {
                if(widgetRepository.existsByIndexZ(widget.getIndexZ())){
                    widgetRepository.shiftAllIndexZAfter(widget.getIndexZ());
                }
            }
        }

        oldWidget.setCoordinationX(widget.getCoordinationX());
        oldWidget.setCoordinationY(widget.getCoordinationY());
        oldWidget.setWidth(widget.getWidth());
        oldWidget.setHeight(widget.getHeight());

        return widgetRepository.save(oldWidget).toConvertWidgetDto();
    }

    @Override
    public void deleteWidget(int widgetId) {
        logger.debug("delete widget call with Id :{}",widgetId);

        if(widgetRepository.findById(widgetId).isPresent()){
            widgetRepository.deleteById(widgetId);
        }else{
            throw  new ApiException(new ApiError("Widget not found for this id : " + widgetId));
        }
    }

    @Override
    public List<WidgetDto> getWidgets() {
        return widgetRepository.findAllByOrderByIndexZAscAsWidgetDto();
    }

}
