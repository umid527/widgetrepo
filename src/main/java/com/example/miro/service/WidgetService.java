package com.example.miro.service;

import com.example.miro.dto.WidgetDto;
import com.example.miro.dto.WidgetInsertDto;

import java.util.List;


public interface WidgetService {

    WidgetDto createWidget(WidgetInsertDto widget);

    WidgetDto findById(int widgetId);

    WidgetDto updateWidget(int widgetId, WidgetInsertDto widget);

    void deleteWidget(int widgetId);

    List<WidgetDto> getWidgets();
}
