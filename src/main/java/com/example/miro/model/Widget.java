package com.example.miro.model;

import com.example.miro.dto.WidgetDto;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "WIDGET")
public class Widget {

    private Integer coordinationX;
    private Integer coordinationY;
    private Integer indexZ;
    private Integer width;
    private Integer height;
    private Integer id;
    private Date lastModificationDate;

    public Widget() {
    }

    public Widget(Integer coordinationX, Integer coordinationY, Integer indexZ, Integer width, Integer height, Integer id, Date lastModificationDate) {
        this.coordinationX = coordinationX;
        this.coordinationY = coordinationY;
        this.indexZ = indexZ;
        this.width = width;
        this.height = height;
        this.id = id;
        this.lastModificationDate = lastModificationDate;
    }
    public Widget(Integer coordinationX, Integer coordinationY, Integer indexZ, Integer width, Integer height) {
        this.coordinationX = coordinationX;
        this.coordinationY = coordinationY;
        this.indexZ = indexZ;
        this.width = width;
        this.height = height;
    }

    @Column(name = "coordinate_x", nullable = false)
    public Integer getCoordinationX() {
        return coordinationX;
    }

    public void setCoordinationX(Integer coordinationX) {
        this.coordinationX = coordinationX;
    }

    @Column(name = "coordinate_y", nullable = false)
    public Integer getCoordinationY() {
        return coordinationY;
    }

    public void setCoordinationY(Integer coordinationY) {
        this.coordinationY = coordinationY;
    }

    @Column(name = "index_z",unique=true, nullable = false)
    public Integer getIndexZ() {
        return indexZ;
    }

    public void setIndexZ(Integer indexZ) {
        this.indexZ = indexZ;
    }

    @Column(name = "width", nullable = false)
    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    @Column(name = "height", nullable = false)
    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "last_mod_date")
    public Date getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }
    @PrePersist
    protected void prePersist() {
        if (this.lastModificationDate == null) lastModificationDate = new Date();
    }

    @PreUpdate
    protected void preUpdate() {
        this.lastModificationDate = new Date();
    }

    public WidgetDto toConvertWidgetDto(){
        WidgetDto widgetDto = new WidgetDto();
        widgetDto.setCoordinationX(this.coordinationX);
        widgetDto.setCoordinationY(this.coordinationY);
        widgetDto.setIndexZ(this.indexZ);
        widgetDto.setHeight(this.height);
        widgetDto.setWidth(this.width);
        widgetDto.setId(this.id);
        widgetDto.setLastModificationDate(this.lastModificationDate);
        return widgetDto;
    }
}
