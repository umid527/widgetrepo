package com.example.miro.controller;

import com.example.miro.dto.WidgetDto;
import com.example.miro.dto.WidgetInsertDto;
import com.example.miro.service.WidgetService;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/miro")
public class WidgetController {

    private final WidgetService widgetService;

    public WidgetController(WidgetService widgetService) {
        this.widgetService = widgetService;
    }

    @GetMapping("/widgets/{id}")
    public ResponseEntity<WidgetDto> findById(@PathVariable(value = "id") int widgetId)
            throws ResourceNotFoundException {
        WidgetDto widgetDto = widgetService.findById(widgetId);
        return ResponseEntity.ok().body(widgetDto);
    }
    @GetMapping("/widgets")
    public ResponseEntity<List<WidgetDto>> getWidgets() {
        List<WidgetDto> widgetDto = widgetService.getWidgets();
        return ResponseEntity.ok().body(widgetDto);
    }

    @PostMapping("/widget")
    public ResponseEntity<WidgetDto> createWidget(@Valid @RequestBody WidgetInsertDto widgetInsertDto) {
        WidgetDto widgetDto =  widgetService.createWidget(widgetInsertDto);
        return ResponseEntity.ok().body(widgetDto);
    }

    @PutMapping("/widget/{id}")
    public WidgetDto updateWidget(@PathVariable(value = "id") int widgetId, @Valid @RequestBody WidgetInsertDto widgetInsertDto) {
        return widgetService.updateWidget(widgetId,widgetInsertDto);
    }

    @DeleteMapping("/widget/{id}")
    public ResponseEntity<?> deleteWidget(@PathVariable(value = "id") int widgetId) {
        widgetService.deleteWidget(widgetId);
        return ResponseEntity.noContent().build();
    }
}