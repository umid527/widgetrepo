package com.example.miro.dto;

import java.io.Serializable;
import java.util.Date;

public class WidgetDto implements Serializable {

    private Integer coordinationX;
    private Integer coordinationY;
    private Integer indexZ;
    private Integer width;
    private Integer height;
    private Integer id;
    private Date lastModificationDate;


    public WidgetDto() {
    }

    public WidgetDto(Integer coordinationX,
                     Integer coordinationY,
                     Integer indexZ,
                     Integer width,
                     Integer height,
                     Integer id,
                     Date lastModificationDate) {
        this.coordinationX = coordinationX;
        this.coordinationY = coordinationY;
        this.indexZ = indexZ;
        this.width = width;
        this.height = height;
        this.id = id;
        this.lastModificationDate = lastModificationDate;
    }

    public Integer getCoordinationX() {
        return coordinationX;
    }

    public void setCoordinationX(Integer coordinationX) {
        this.coordinationX = coordinationX;
    }

    public Integer getCoordinationY() {
        return coordinationY;
    }

    public void setCoordinationY(Integer coordinationY) {
        this.coordinationY = coordinationY;
    }

    public Integer getIndexZ() {
        return indexZ;
    }

    public void setIndexZ(Integer indexZ) {
        this.indexZ = indexZ;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }


    @Override
    public String toString() {
        return "WidgetDto{" +
                "coordinationX=" + coordinationX +
                ", coordinationY=" + coordinationY +
                ", indexZ=" + indexZ +
                ", width=" + width +
                ", height=" + height +
                ", id=" + id +
                ", lastModificationDate=" + lastModificationDate +
                '}';
    }
}
