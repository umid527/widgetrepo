package com.example.miro.dto;

import com.example.miro.model.Widget;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class WidgetInsertDto implements Serializable {

    @NotNull(message = "Please specify the widget's x coordinate")
    private Integer coordinationX;

    @NotNull(message = "Please specify the widget's y coordinate")
    private Integer coordinationY;
    private Integer indexZ;

    @NotNull(message = "Please specify the widget's width")
    private Integer width;
    @NotNull(message = "Please specify the widget's height")
    private Integer height;

    public WidgetInsertDto(Integer coordinationX, Integer coordinationY, Integer indexZ, Integer width, Integer height) {
        this.coordinationX = coordinationX;
        this.coordinationY = coordinationY;
        this.indexZ = indexZ;
        this.width = width;
        this.height = height;
    }

    public WidgetInsertDto() {
    }

    public Integer getCoordinationX() {
        return coordinationX;
    }

    public void setCoordinationX(Integer coordinationX) {
        this.coordinationX = coordinationX;
    }

    public Integer getCoordinationY() {
        return coordinationY;
    }

    public void setCoordinationY(Integer coordinationY) {
        this.coordinationY = coordinationY;
    }

    public Integer getIndexZ() {
        return indexZ;
    }

    public void setIndexZ(Integer indexZ) {
        this.indexZ = indexZ;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "WidgetInsertDto{" +
                "coordinationX=" + coordinationX +
                ", coordinationY=" + coordinationY +
                ", indexZ=" + indexZ +
                ", width=" + width +
                ", height=" + height +
                '}';
    }

    public Widget toConvertWidget(){
        Widget widget = new Widget();
        widget.setCoordinationX(this.coordinationX);
        widget.setCoordinationY(this.coordinationY);
        widget.setIndexZ(this.indexZ);
        widget.setHeight(this.height);
        widget.setWidth(this.width);
        return widget;
    }
}
