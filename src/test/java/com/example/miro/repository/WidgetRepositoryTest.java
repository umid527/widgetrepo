package com.example.miro.repository;

import com.example.miro.dto.WidgetDto;
import com.example.miro.model.Widget;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class WidgetRepositoryTest {

    @Autowired
    private WidgetRepository widgetRepository;
    @Autowired
    private TestEntityManager entityManager;

    @Test
    @DisplayName("FindById should return the saved widget")
    void FindById_ShouldReturnSavedWidget() {
        Widget widget=new Widget(1,1,1,1,1);
        Widget savedWidgetEntity = widgetRepository.save(widget);
        Widget widgetEntityFromDb = widgetRepository.findById(savedWidgetEntity.getId()).get();

        assertEquals(savedWidgetEntity,widgetEntityFromDb);
    }
    @Test
    @DisplayName("ExistsByIndexZ should return true when indexZ value already exists in DB")
    void ExistsByIndexZ_ShouldReturnTrue_WhenIndexZExistsInDB() {
        Widget widget=new Widget(1,1,1,1,1);
        widgetRepository.save(widget);

        assertTrue(widgetRepository.existsByIndexZ(widget.getIndexZ()));
    }
    @Test
    @DisplayName("ExistsByIndexZ should return false when indexZ value does not exist in DB")
    void ExistsByIndexZ_ShouldReturnFalse_WhenIndexZDoesNotExistInDB() {
        Widget widget=new Widget(1,1,1,1,1);
        widgetRepository.save(widget);

        assertFalse(widgetRepository.existsByIndexZ(widget.getIndexZ()+1));
    }
    @Test
    @DisplayName("possibleUsableIndexZ should return highest possible id")
    void PossibleUsableIndexZ_ShouldReturnHighestPossibleId() {
        Widget widget=new Widget(1,1,1,1,1);
        widgetRepository.save(widget);

        assertEquals(widget.getIndexZ()+1, (int) widgetRepository.possibleUsableIndexZ());
    }

    @Test
    @DisplayName("ShiftAllIndexZAfter should shift all widgets' indexZ foreground")
    void ShiftAllIndexZAfter_ShouldShiftAllWidgetsindexZForeground() {
        Widget widget=new Widget(1,1,1,1,1);
        widgetRepository.save(widget);

        Widget widgetEntityFromDb = widgetRepository.findById(widget.getId()).get();
        int beforeShiftIndexZ=widgetEntityFromDb.getIndexZ();
        widgetRepository.shiftAllIndexZAfter(widget.getIndexZ());
        entityManager.refresh(widgetEntityFromDb);
        int afterShiftIndexZ=widgetEntityFromDb.getIndexZ();
        assertEquals(beforeShiftIndexZ + 1,afterShiftIndexZ);
    }

    @Test
    @DisplayName("FindAllByOrderByIndexZAscAsWidgetDto should return all widgets ordered by indexZ")
    void FindAllByOrderByIndexZAscAsWidgetDto_ShouldReturnAllWidgetsOrderedByIndexZ() {
        Widget widget=new Widget(1,1,1,1,1);
        Widget widget2=new Widget(1,1,3,1,1);
        Widget widget3=new Widget(1,1,2,1,1);
        widgetRepository.save(widget);
        widgetRepository.save(widget2);
        widgetRepository.save(widget3);

        List<WidgetDto> widgetsOrdered = widgetRepository.findAllByOrderByIndexZAscAsWidgetDto();

        assertEquals(widgetsOrdered.get(0).getIndexZ(), widget.getIndexZ());
        assertAll("Should index z descendin order",
                ()->assertEquals(widgetsOrdered.get(0).getIndexZ(), widget.getIndexZ()),
                ()->assertEquals(widgetsOrdered.get(1).getIndexZ(), widget3.getIndexZ()),
                ()->assertEquals(widgetsOrdered.get(2).getIndexZ(), widget2.getIndexZ()));
    }



}
