package com.example.miro.service;

import com.example.miro.dto.WidgetDto;
import com.example.miro.dto.WidgetInsertDto;
import com.example.miro.errorhandling.ApiException;
import com.example.miro.model.Widget;
import com.example.miro.repository.WidgetRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@SpringBootTest
@ExtendWith(MockitoExtension.class)
class WidgetServiceImplTest1 {

    @InjectMocks
    WidgetServiceImpl widgetService;

    @Mock
    private WidgetRepository widgetRepository;

    private Widget widgetRepoObject;
    private WidgetInsertDto widgetInsert;

    @BeforeEach
    void setUp() {
        widgetInsert = new WidgetInsertDto(1,1,1,1,1);
        widgetRepoObject = new Widget(1,1,1,1,1,1,new Date());
    }

    @Test
    @DisplayName("CreateWidget should save widget when indexZ is null")
    void CreateWidget_ShouldSaveWidget_WhenIndexZIsNull() {
        when(widgetRepository.save(any())).thenReturn(widgetRepoObject);
        widgetInsert.setIndexZ(null); // null value
        when(widgetRepository.possibleUsableIndexZ()).thenReturn(1);

        WidgetDto widgetDtoFindBy = widgetService.createWidget(widgetInsert);

        widgetInsert.setIndexZ(1);
        assertThat(toWidgetInsert(widgetDtoFindBy),samePropertyValuesAs(widgetInsert));
        verify(widgetRepository, times(0)).existsByIndexZ(any());
        verify(widgetRepository, times(1)).possibleUsableIndexZ();
        verify(widgetRepository, times(1)).save(any(Widget.class));
        verify(widgetRepository, times(0)).shiftAllIndexZAfter(any());
    }

    @Test
    @DisplayName("CreateWidget should save widget when indexZ is not null and indexZ does not exist")
    void CreateWidget_ShouldSaveWidget_WhenIndexZIsNotNullAndIndexZNotExist() {
        when(widgetRepository.save(any())).thenReturn(widgetRepoObject);
        widgetInsert.setIndexZ(1); // not null value
        when(widgetRepository.existsByIndexZ(any())).thenReturn(false);


        WidgetDto widgetDtoFindBy = widgetService.createWidget(widgetInsert);

        assertThat(toWidgetInsert(widgetDtoFindBy),samePropertyValuesAs(widgetInsert));

        verify(widgetRepository, times(1)).existsByIndexZ(any());
        verify(widgetRepository, times(0)).possibleUsableIndexZ();
        verify(widgetRepository, times(1)).save(any(Widget.class));
        verify(widgetRepository, times(0)).shiftAllIndexZAfter(any());
    }

    @Test
    @DisplayName("CreateWidget should save widget when indexZ is not null and indexZ exists")
    void CreateWidget_ShouldSaveWidget_WhenIndexZIsNotNullAndIndexZExists() {
        when(widgetRepository.save(any())).thenReturn(widgetRepoObject);
        widgetInsert.setIndexZ(1); // not null value
        when(widgetRepository.existsByIndexZ(any())).thenReturn(true);


        WidgetDto widgetDtoFindBy = widgetService.createWidget(widgetInsert);
        assertThat(toWidgetInsert(widgetDtoFindBy),samePropertyValuesAs(widgetInsert));
        verify(widgetRepository, times(1)).existsByIndexZ(any());
        verify(widgetRepository, times(0)).possibleUsableIndexZ();
        verify(widgetRepository, times(1)).save(any(Widget.class));
        verify(widgetRepository, times(1)).shiftAllIndexZAfter(any());
    }


    @Test
    @DisplayName("FindById should return the widget when id exists")
    void FindById_ShouldReturnWidget_WhenIdExists() {
        when(widgetRepository.findById(anyInt())).thenReturn(Optional.of(widgetRepoObject));
        WidgetDto widgetDtoFindBy = widgetService.findById(anyInt());

        assertThat(widgetRepoObject.toConvertWidgetDto(),samePropertyValuesAs(widgetDtoFindBy));
        verify(widgetRepository, times(1)).findById(any());
    }

    @Test
    @DisplayName("FindById should throw API exception when id does not exist")
    void FindById_ShouldThrowApiException_WhenIdDoesNotExists() {
        when(widgetRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(
                ApiException.class,
                () -> { widgetService.findById(anyInt()); }
        );
    }

    @Test
    @DisplayName("DeleteWidget should call once")
    void DeleteWidget_ShouldCallOnce() {
        when(widgetRepository.findById(anyInt())).thenReturn(Optional.of(widgetRepoObject));
        doNothing().when(widgetRepository).deleteById(anyInt());
        widgetService.deleteWidget(anyInt());
        verify(widgetRepository,times(1)).deleteById(anyInt());
    }

    @Test
    @DisplayName("GetWidgets should return widget list")
    void GetWidgets_ShouldReturnWidgetList() {
        List<WidgetDto> widgetDtoList=new ArrayList<>();
        WidgetDto widget1 = new WidgetDto(1,1,1,1,1,1,new Date());
        WidgetDto widget2 = new WidgetDto(1,1,2,1,1,2,new Date());
        widgetDtoList.add(widget1);
        widgetDtoList.add(widget2);

        when(widgetRepository.findAllByOrderByIndexZAscAsWidgetDto()).thenReturn(widgetDtoList);

        assertEquals(widgetDtoList, widgetService.getWidgets());
    }

    private WidgetInsertDto toWidgetInsert(WidgetDto widgetDto) {
        WidgetInsertDto widgetInsertDto=new WidgetInsertDto(widgetDto.getCoordinationX(),
                widgetDto.getCoordinationY(),
                widgetDto.getIndexZ(),
                widgetDto.getWidth(),
                widgetDto.getHeight());
        return widgetInsertDto;
    }
}