package com.example.miro.controller;

import com.example.miro.dto.WidgetDto;
import com.example.miro.service.WidgetServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
class WidgetControllerTest {

    @MockBean
    private WidgetServiceImpl widgetService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @DisplayName("GET /api/miro/widgets/{id} - 200_OK")
    void FindById_ShouldReturnTheWidget() throws Exception {
        WidgetDto widgetDto = new WidgetDto(1,1,1,1,1,1,new Date());
        when(widgetService.findById(1)).thenReturn(widgetDto);

        mockMvc.perform(get("/api/miro/widgets/{id}",1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))

                .andExpect(jsonPath("$.coordinationX", equalTo(widgetDto.getCoordinationX())))
                .andExpect(jsonPath("$.coordinationY", equalTo(widgetDto.getCoordinationY())))
                .andExpect(jsonPath("$.indexZ", equalTo(widgetDto.getIndexZ())))
                .andExpect(jsonPath("$.width", equalTo(widgetDto.getWidth())))
                .andExpect(jsonPath("$.height", equalTo(widgetDto.getId())))
                .andExpect(jsonPath("$.id", equalTo(widgetDto.getId())));
    }
}